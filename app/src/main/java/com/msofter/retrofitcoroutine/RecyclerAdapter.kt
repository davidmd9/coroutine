package com.msofter.retrofitcoroutine

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.msofter.retrofitcoroutine.api.Offer
import com.squareup.picasso.Picasso

class RecyclerAdapter(items: List<Offer>, context: Context) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    var items: List<Offer>? = null
    var context: Context? = null

    init {
        this.items = items
        this.context = context
    }

    public fun setNewItems(items:List<Offer>){
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return items?.size!!
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.get().load(items!![position].image).into(holder.image)
        holder.textView?.text = items!![position].button_text

    }

    inner class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textView:TextView? = null
        var image:ImageView? = null
        init {
            textView = itemView.findViewById(R.id.tvText)
            image = itemView.findViewById(R.id.ivImage)
        }
    }
}