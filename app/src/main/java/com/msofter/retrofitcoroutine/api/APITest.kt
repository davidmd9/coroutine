package com.msofter.retrofitcoroutine.api

import retrofit2.Call
import retrofit2.http.GET


data class ServerResponce (
    val error: Any? = null,
    val success: Boolean,
    val data: List<Offer>
)

data class Offer (
    val id: Long,
    val companyID: Long,
    val date: Long,
    val rate: String,
    val text: String,
    val sum: String,
    val button_text: String,
    val button_url: String,
    val image: String
)
